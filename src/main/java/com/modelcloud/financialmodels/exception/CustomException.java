package com.modelcloud.financialmodels.exception;



/**
 * The Class CustomException.
 * 
 * @author Ravi
 */
public class CustomException extends Exception {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 862073793703082401L;

	/**
	 * The code.
	 */
	private String code;

	/**
	 * The message.
	 */
	private String message;

	/**
	 * Instantiates a new custom exception.
	 *
	 * @param error
	 *            the error
	 */
	public CustomException(com.modelcloud.financialmodels.utils.Error error) {
		this.message = error.getMessage();
		this.code = error.getCode();
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

}