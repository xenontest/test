package com.modelcloud.financialmodels.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * The Class GlobalExceptionHandler.
 * 
 * @author Ravi
 */
@ControllerAdvice
public class GlobalExceptionHandler {

	/**
	 * The Constant LOG.
	 */
	// private static final Log LOG =
	// LogFactory.getLog(GlobalExceptionHandler.class);

	/**
	 * Exception.
	 *
	 * @param exception
	 *            the exception
	 * @return the response entity
	 */
	@ExceptionHandler(Exception.class)
	public @ResponseBody ResponseEntity<?> exception(Exception exception) {
		if ((exception instanceof CustomException)) {
			return new ResponseEntity<Object>(exception, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Object>(HttpStatus.BAD_REQUEST);
	}
}
