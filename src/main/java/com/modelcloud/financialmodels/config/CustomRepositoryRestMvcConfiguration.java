package com.modelcloud.financialmodels.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.core.event.ValidatingRepositoryEventListener;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;
import org.springframework.validation.Validator;

@Configuration
public class CustomRepositoryRestMvcConfiguration extends RepositoryRestMvcConfiguration {

	@Autowired
	private Validator validator;
	
//	@Override
//	protected void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
//		// TODO Auto-generated method stub
//		super.configureRepositoryRestConfiguration(config);
//		try {
//			config.setBasePath("/api");
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
	
	@Override
	protected void configureValidatingRepositoryEventListener(ValidatingRepositoryEventListener validatingListener) {
		validatingListener.addValidator("beforeCreate", validator);
		validatingListener.addValidator("beforeSave", validator);
	}
}
