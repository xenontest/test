package com.modelcloud.financialmodels.config;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.web.WebMvcAutoConfiguration.WebMvcAutoConfigurationAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.multipart.support.MultipartFilter;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.modelcloud.financialmodels.App;
import com.modelcloud.financialmodels.utils.PropertiesFile;
import com.mongodb.MongoClient;

@Configuration
public class MvcConfiguration extends WebMvcAutoConfigurationAdapter {

	static PropertiesFile propertiesFile;
	static {
		try {
			propertiesFile = new PropertiesFile();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		try {
			registry.addResourceHandler("/fin-models/**")
					.addResourceLocations("file:" + propertiesFile.getFinancialUploadedModels());
		} catch (Exception e) {
			System.out.println("addResourceHandlers " + e.getMessage());
		}
		super.addResourceHandlers(registry);
	}

	@Bean
	public MongoTemplate mongoTemplate() throws Exception {

		MongoTemplate mongoTemplate = new MongoTemplate(new MongoClient("127.0.0.1"),
				System.getenv("MODELCLOUD_API_DATABASE_NAME"));

		return mongoTemplate;
	}

	@Bean(name = "multipartResolver")
	public CommonsMultipartResolver multipartResolver() {
		CommonsMultipartResolver resolver = new CommonsMultipartResolver();
		resolver.setMaxInMemorySize(Integer.parseInt(System.getenv("MODELCLOUD_API_MULTIPART_MAX_MEMORY_SIZE")));
		resolver.setMaxUploadSize(Integer.parseInt(System.getenv("MODELCLOUD_API_MULTIPART_MAX_MEMORY_SIZE")));
		return resolver;
	}

	@Bean
	@Order(0)
	public MultipartFilter multipartFilter() {
		MultipartFilter multipartFilter = new MultipartFilter();
		multipartFilter.setMultipartResolverBeanName("multipartResolver");
		return multipartFilter;
	}

	@Bean
	public Properties getMailProperties() {
		Properties properties = new Properties();
		properties.setProperty("mail.transport.protocol", "smtp");
		properties.setProperty("mail.smtp.auth", "true");
		properties.setProperty("mail.smtp.starttls.enable", "true");
		return properties;
	}

	@Bean
	public JavaMailSender javaMailSender() {
		JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
		try {
			javaMailSender.setHost(System.getenv("MODELCLOUD_API_EMAIL_HOSTNAME"));
			javaMailSender.setPort(Integer.parseInt(System.getenv("MODELCLOUD_API_EMAIL_PORTNUMBER")));
			javaMailSender.setUsername(System.getenv("MODELCLOUD_API_EMAILID"));
			javaMailSender.setPassword(System.getenv("MODELCLOUD_API_EMAIL_PASSWORD"));
			javaMailSender.setJavaMailProperties(getMailProperties());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return javaMailSender;
	}
}
