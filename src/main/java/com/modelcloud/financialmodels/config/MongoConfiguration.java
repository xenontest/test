package com.modelcloud.financialmodels.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;

import com.modelcloud.financialmodels.constants.Constants;
import com.mongodb.Mongo;

@Configuration
public class MongoConfiguration extends AbstractMongoConfiguration implements Constants{

//	@Value("${mongo.database}")
//	private String mongoDatabaseName;

//	@Value("${mongo.hostname}")
//	private String mongoHostName;
	
	@Override
	protected String getDatabaseName() 
	{
		return System.getenv("MODELCLOUD_API_DATABASE_NAME");
	}

	
	@Bean
	public Mongo mongo() throws Exception 
	{
		return new Mongo(System.getenv("MODELCLOUD_API_MONGO_HOSTNAME"));
	}
	
	@Override
	public String getMappingBasePackage() 
	{
		return BASE_PACKAGE_NAME;
	}

}
