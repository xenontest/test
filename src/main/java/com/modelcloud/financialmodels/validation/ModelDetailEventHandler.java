package com.modelcloud.financialmodels.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleBeforeSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;

import com.modelcloud.financialmodels.entities.ModelDetails;
import com.modelcloud.financialmodels.exception.CustomException;
import com.modelcloud.financialmodels.repositories.ModelDetailRepository;
import com.modelcloud.financialmodels.repositories.UserRepository;
import com.modelcloud.financialmodels.utils.Error;

@Component
@RepositoryEventHandler(ModelDetails.class)
public class ModelDetailEventHandler  {
	
	
	@Autowired
	ModelDetailRepository modelDetailRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@HandleBeforeSave
	public void handleBeforeSave(ModelDetails modelDetails) throws CustomException{
	
		if(userRepository.findOne(modelDetails.getEmpId()) == null){
			throw new  CustomException(Error.INVALID_USER_ID);
		}
		
	}

}
