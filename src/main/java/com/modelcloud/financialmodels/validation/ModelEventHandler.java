package com.modelcloud.financialmodels.validation;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;

import com.modelcloud.financialmodels.entities.ModelDetails;
import com.modelcloud.financialmodels.repositories.ModelDetailRepository;

@Component
@RepositoryEventHandler(ModelDetails.class)
public class ModelEventHandler {

	@Autowired
	ModelDetailRepository modelDetailRepository;

	/* Created for checking if user allready exist or not........ */

	@HandleBeforeCreate
	public void handleBeforeCreate(ModelDetails modelDetails) {
		System.out.println("MOdelDetails  " + modelDetails);
		// ModelDetails modelDetailRef =
		// modelDetailRepository.findByEmpIdIgnoreCase(modelDetails.getEmpId());
		if (modelDetails.getDateCreatedAt() == null)
			modelDetails.setDateCreatedAt(new Date());
//		modelDetails.setIsDeleted(0);
//		modelDetails.setStatus(0);
	}
}
