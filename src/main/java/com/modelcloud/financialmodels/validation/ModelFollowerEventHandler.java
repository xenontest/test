package com.modelcloud.financialmodels.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleBeforeSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;

import com.modelcloud.financialmodels.entities.CompanyModelFollowers;
import com.modelcloud.financialmodels.entities.ModelDetails;
import com.modelcloud.financialmodels.exception.CustomException;
import com.modelcloud.financialmodels.repositories.ModelDetailRepository;
import com.modelcloud.financialmodels.repositories.ModelFollowerRepository;
import com.modelcloud.financialmodels.repositories.UserRepository;
import com.modelcloud.financialmodels.utils.Error;

@Component
@RepositoryEventHandler(CompanyModelFollowers.class)
public class ModelFollowerEventHandler  {
	
	@Autowired
	ModelFollowerRepository modelFollowerRepository;
	
	@Autowired
	ModelDetailRepository modelDetailRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@HandleBeforeSave
	public void handleBeforeSave(CompanyModelFollowers companyModelFollowers) throws CustomException{
		if (modelDetailRepository.findOne(companyModelFollowers.getModelId()) == null) {
			throw new CustomException(Error.INVALID_MODELID);
		}
		if(userRepository.findOne(companyModelFollowers.getUserId()) == null){
			throw new  CustomException(Error.INVALID_USER_ID);
		}
		
	}

}
