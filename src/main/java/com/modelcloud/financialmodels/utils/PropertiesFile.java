package com.modelcloud.financialmodels.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import org.springframework.stereotype.Component;

import com.modelcloud.financialmodels.constants.Constants;

@Component
public class PropertiesFile implements Constants {

//	Properties prop = new Properties();
//
//	public PropertiesFile() throws Exception {
//		loadProperties();
//	}
//
//	public void loadProperties() throws Exception {
//		InputStream inputStream = new FileInputStream(new File(System.getenv(MODELCLOUD_VARIABLE)));
//		if (inputStream != null) {
//			prop.load(inputStream);
//		}
//
//	}

	public String getFinancialUploadedModels() {
		return System.getenv("MODELCLOUD_API_FINANCIAL_UPLOAD_MODELS");
	}

	public String getDownloadedHistoryPath() {
		return System.getenv("MODELCLOUD_API_DOWNLOAD_HISTORY_MODELS");
	}

	public String getRegisteredUserApiKey() throws Exception {
		return System.getenv("MODELCLOUD_API_APIKEY_REGISTERED_USER");
	}

	public String getsUserApiKey() throws Exception {
		return System.getenv("MODELCLOUD_API_APIKEY_USER");
	}

	public String getAdminApiKey() throws Exception {
		return System.getenv("MODELCLOUD_API_APIKEY_ADMIN");
	}

	public String getEmployeeApiKey() throws Exception {
		return System.getenv("MODELCLOUD_API_APIKEY_EMPLOYEE");
	}

	public String getCOMPANY() {
		return System.getenv("MODELCLOUD_API_COMPANY_NAME");
	}

	public String getEMAILID() {
		return System.getenv("MODELCLOUD_API_EMAILID");
	}

	public String getUserOwnModel() throws Exception {
		return System.getenv("MODELCLOUD_API_USER_OWN_MODEL");
	}

	public String getAwsS3BucketName() throws Exception {
		return System.getenv("MODELCLOUD_API_AWS_S3_BUCKETNAME");
	}
}
