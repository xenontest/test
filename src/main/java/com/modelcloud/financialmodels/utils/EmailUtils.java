package com.modelcloud.financialmodels.utils;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

@Component
public class EmailUtils {

	@Autowired
	JavaMailSender javaMailSender;
	
	@Autowired
	PropertiesFile propertiesFile;
	
	final static int NO_OF_CONCURRENT_THREADS = 10;
	private static final Executor THREAD_POOL = Executors.newFixedThreadPool(NO_OF_CONCURRENT_THREADS);
	
	public void sendMail(String companyName, String title, String emailId, String comment) throws Exception {
		
		StringBuilder textmessage = new StringBuilder("<p>Dear User, ").append(companyName).append(" ").append("has been updated their " )
				.append(title).append(" model<br/>").append(" <b>Updations:-</b><br/>").append(comment+" <br/>").append("Thank You<p/>");
				
		MimeMessage message = javaMailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message, true);
		helper.setFrom(propertiesFile.getEMAILID());
		helper.setTo(emailId);
		helper.setSubject("Regarding updation in "+ propertiesFile.getCOMPANY()+" " + title+" "+"model");
		message.setContent(textmessage.toString(), "text/html");
		sendEmail(message);
		
	}
	
	private void sendEmail(final MimeMessage mimeMessage) {
		THREAD_POOL.execute(new Runnable() {

			@Override
			public void run() {
				javaMailSender.send(mimeMessage);
			}
		});
	}
}
