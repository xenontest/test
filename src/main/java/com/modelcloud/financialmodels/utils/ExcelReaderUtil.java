package com.modelcloud.financialmodels.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Header;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.tomcat.jni.File;
import org.springframework.stereotype.Component;

import com.modelcloud.financialmodels.entities.ModelDetails;
import com.modelcloud.financialmodels.entities.QuartorRecord;
import com.mysql.fabric.xmlrpc.base.Array;
import com.google.gson.Gson;

@Component
public class ExcelReaderUtil {

	public Map excelReader(String modelPath, String requiredSheetName) throws Exception {
		InputStream inp = null;

		Double minYear = 0.0;
		String sheetname = "";
		String sheetname1="";

		SortedMap<Integer, List<String>> map = new TreeMap<Integer, List<String>>();

		try {
			inp = new FileInputStream(modelPath);

			Workbook wb = WorkbookFactory.create(inp);
			int loopLimit = wb.getNumberOfSheets();
			for (int noOfSheet = 0; noOfSheet < loopLimit; noOfSheet++) {

				// for (int sizeOfsheetsToAdd = 0; sizeOfsheetsToAdd <
				// sheetsNotToAdd.length; sizeOfsheetsToAdd++) {
				// if (sheetsNotToAdd[sizeOfsheetsToAdd] == noOfSheet) {
				// continue;
				// }
				// }

				// System.out.println("####### "+new
				// java.io.File(modelPath).exists());

				Sheet sheet = wb.getSheetAt(noOfSheet);

				sheetname = wb.getSheetName(noOfSheet);

				Header header = sheet.getHeader();
				
				if (sheetname.equals(requiredSheetName)) {

					sheetname1=sheetname;
					System.out.println(sheetname);

					int rowsCount = sheet.getLastRowNum();

					for (int i = 0; i <= rowsCount; i++) {
						Row row = sheet.getRow(i);

						if (row != null) {

							int colCounts = row.getLastCellNum();

							for (int j = 0; j < colCounts; j++) {

								Cell cell = row.getCell(j);
								List list = map.get(j);
								Object value = null;
								if (cell != null) {

									if (list == null) {
										list = new ArrayList<String>();
									}
									if (cell.getCellTypeEnum() == CellType.STRING) {

										value = (cell.getStringCellValue());

									} else if (cell.getCellTypeEnum() == CellType.NUMERIC) {

										if (cell.getCellStyle().getDataFormatString().contains("%")) {

											value = cell.getNumericCellValue() * 100;

										} else {

											value = cell.getNumericCellValue();

										}

										// value = (cell.getNumericCellValue());

									} else if (cell.getCellTypeEnum() == CellType.BOOLEAN) {

										value = (cell.getBooleanCellValue());

									} else if (cell.getCellTypeEnum() == CellType.BLANK) {

										value = (" ");

									} else if (cell.getCellTypeEnum() == CellType.FORMULA) {

										switch (cell.getCachedFormulaResultType()) {

										case Cell.CELL_TYPE_NUMERIC:

											if (cell.getCellStyle().getDataFormatString().contains("%")) {

												value = cell.getNumericCellValue() * 100;

											} else {

												value = cell.getNumericCellValue();

											}

											// value =
											// (cell.getNumericCellValue());

											break;

										case Cell.CELL_TYPE_STRING:

											value = (cell.getStringCellValue());
											break;

										default:
											value = ("");
										}
									} else {
										value = ("");
									}
									list.add(value);
									map.put(j, list);

									if (colCounts == 1) {

										for (int k = 1; k <= 1024; k++) {
											List<String> otherData = map.get(k);
											if (otherData == null)
												otherData = new ArrayList<String>();
											otherData.add(" ");
											map.put(k, otherData);
										}
									}
									
									if (validateYear(value) && minYear == 0) {
										minYear = Double.parseDouble(value + "");

									}
								}

							}
//							System.out.println("111= "+sheetname);

						}
					}
					
				}
//				System.out.println("1111 = "+sheetname);
//				System.out.println("s1 = "+s);
			}
//			System.out.println("2222 = "+sheetname);
//			System.out.println("s2 = "+s);

//			System.out.println("above catch = "+sheetname);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				inp.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
//		System.out.println("above finally = "+sheetname);
		return convertMap(minYear, map, sheetname1);
	}

	public static Map convertMap(double minYear, SortedMap<Integer, List<String>> map, String sheetname) {

		// Map<Map<String, String>, List<String>> map1 = new HashMap<Map<String,
		// String>, List<String>>();
		
		Map<String, Object> resultMap = new HashMap<String, Object>();

		List<QuartorRecord> list = new ArrayList();
		String currentYear = "";
		Iterator<Integer> itr = map.keySet().iterator();
		int startIndex = 0;
		while (itr.hasNext()) {
			List<String> values = map.get(itr.next());

			if (values != null) {

				QuartorRecord quartorRecord = new QuartorRecord();

				String quarter = "";
//				String sheetNam = "";
				boolean isIncremented=false;
				for (int j = 0; j < values.size(); j++) {
					if (String.valueOf(values.get(j)).equals("Q1")
							|| (String.valueOf(values.get(j)).toLowerCase().contains("mar'"))) {
						quarter = "Q1";
//						sheetNam = sheetname;
						if (!isIncremented) {
							if ((String.valueOf(values.get(j)).equals("Q1")))
								startIndex = j + 2;
							else if ((String.valueOf(values.get(j)).toLowerCase().contains("mar'")))
								startIndex = j + 1;
							currentYear = String.valueOf((double) minYear);
							minYear++;
							isIncremented = true;
						}
					} else if (String.valueOf(values.get(j)).equals("Q2")||(String.valueOf(values.get(j)).toLowerCase().contains("jun'"))) {
						quarter = "Q2";
//						sheetNam = sheetname;

					} else if (String.valueOf(values.get(j)).equals("Q3")||(String.valueOf(values.get(j)).toLowerCase().contains("sept'"))) {
						quarter = "Q3";
//						sheetNam = sheetname;

					} else if (String.valueOf(values.get(j)).equals("Q4")||(String.valueOf(values.get(j)).toLowerCase().contains("dec'"))) {
						quarter = "Q4";
//						sheetNam = sheetname;

					}

				}
				quartorRecord.setCurrentYear(currentYear);
//				quartorRecord.setSheetName(sheetNam);
				quartorRecord.setQuarter(quarter);
				quartorRecord.setValues(values);
//				 System.out.println(quartorRecord.getSheetName());

				list.add(quartorRecord);
				// System.out.println(list);
			}
		}
		// System.out.println("List = " + new Gson().toJson(list));
		resultMap.put("startIndex", startIndex);
		resultMap.put("sheetName", sheetname);
		resultMap.put("results", list);
		return resultMap;
		

	}

	public static boolean validateYear(Object value) {
		for (double i = 1990.0; i <= 2050.0; i++) {

			if (String.valueOf(i).equals(String.valueOf(value).trim())) {
				return true;
			}
		}
		return false;
	}
}
