
package com.modelcloud.financialmodels.utils;


public enum Error {

	/**
	 * The invalid model id.
	 */
	INVALID_MODELID("Please pass valid model id", "invalid_modelId"),

	/**
	 * The invalid user id.
	 */
	INVALID_USER_ID("Please pass valid user detail", "invalid_user");

	
	/**
	 * The code.
	 */
	private final String code;

	/**
	 * The message.
	 */
	private final String message;

	/**
	 * Instantiates a new error.
	 *
	 * @param message
	 *            the message
	 * @param code
	 *            the code
	 */
	Error(String message, String code) {
		this.message = message;
		this.code = code;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

}
