package com.modelcloud.financialmodels;

import java.io.IOException;

import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.FileSystemResource;

import com.modelcloud.financialmodels.constants.Constants;
import com.modelcloud.financialmodels.filter.CorsFilter;
import com.modelcloud.financialmodels.filter.DownloadFilter;

/**
 * Hello world!
 *
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@ServletComponentScan(basePackageClasses = { CorsFilter.class, DownloadFilter.class })
public class App implements Constants {

	public static void main(String[] args) {
		if (System.getenv("MODELCLOUD_API_DATABASE_NAME") == null
				&& System.getenv("MODELCLOUD_API_MONGO_HOSTNAME") == null
				&& System.getenv("MODELCLOUD_API_MONGO_PORTNUMBER") == null
				&& System.getenv("MODELCLOUD_API_EMAILID") == null
				&& System.getenv("MODELCLOUD_API_EMAIL_PASSWORD") == null
				&& System.getenv("MODELCLOUD_API_EMAIL_HOSTNAME") == null
				&& System.getenv("MODELCLOUD_API_EMAIL_PORTNUMBER") == null
				&& System.getenv("MODELCLOUD_API_FINANCIAL_UPLOAD_MODELS") == null
				&& System.getenv("MODELCLOUD_API_DOWNLOAD_HISTORY_MODELS") == null
				&& System.getenv("MODELCLOUD_API_USER_OWN_MODEL") == null
				&& System.getenv("MODELCLOUD_API_APIKEY_REGISTERED_USER") == null
				&& System.getenv("MODELCLOUD_API_APIKEY_USER") == null
				&& System.getenv("MODELCLOUD_API_APIKEY_ADMIN") == null
				&& System.getenv("MODELCLOUD_API_APIKEY_EMPLOYEE") == null
				&& System.getenv("MODELCLOUD_API_COMPANY_NAME") == null
				&& System.getenv("MODELCLOUD_API_AWS_S3_BUCKETNAME") == null
				&& System.getenv("MODELCLOUD_API_MULTIPART_MAX_MEMORY_SIZE") == null) {
			System.err.println("Please add MODELFACTORY_APP variables");
		} else {
			SpringApplication.run(App.class, args);
		}
	}

	/* For Environment Variables */
	// @Bean
	// public PropertyPlaceholderConfigurer propertyPlaceholderConfigurer()
	// throws IOException {
	// PropertyPlaceholderConfigurer propertyPlaceholderConfigurer = new
	// PropertyPlaceholderConfigurer();
	// propertyPlaceholderConfigurer.setLocations(new
	// FileSystemResource(System.getenv(MODELCLOUD_VARIABLE)));
	// propertyPlaceholderConfigurer.setIgnoreUnresolvablePlaceholders(true);
	// return propertyPlaceholderConfigurer;
	// }
}
