package com.modelcloud.financialmodels.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.modelcloud.financialmodels.entities.AccessTokens;

@RepositoryRestResource(collectionResourceRel = "accessToken", path = "accessToken",exported=false)

public interface AccessTokenRepository extends MongoRepository<AccessTokens, String> {

	List<AccessTokens> findByAccessToken(String accessToken);
	
	@RestResource(exported = true)
	AccessTokens findByUserId(String userId);
}
