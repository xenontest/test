package com.modelcloud.financialmodels.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.modelcloud.financialmodels.entities.CompanyModelFollowers;


@RepositoryRestResource(collectionResourceRel = "companyModelFollowers", path = "company-models-followers")
public interface ModelFollowerRepository extends MongoRepository<CompanyModelFollowers, String> {

	@RestResource(exported = true)
	List<CompanyModelFollowers> findByUserId(String userId);
	
	@RestResource(exported = true)
	List<CompanyModelFollowers> findByUserIdOrModelId(String userId, String modelId);
	
	@RestResource(exported = true)
	List<CompanyModelFollowers> findByModelId(String modelId);
	
	@RestResource(exported = true)
	List<CompanyModelFollowers> findByModelIdAndAlertActivated(String modelId, boolean alertActivated);
	
	@Override

	@RestResource(exported = false)
	CompanyModelFollowers save(CompanyModelFollowers arg0);
}
