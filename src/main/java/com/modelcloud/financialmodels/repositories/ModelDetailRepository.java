package com.modelcloud.financialmodels.repositories;

import java.util.Collection;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.modelcloud.financialmodels.entities.ModelDetails;

@RepositoryRestResource(collectionResourceRel = "modeldetails", path = "modeldetails")
public interface ModelDetailRepository extends MongoRepository<ModelDetails, String> {
	
	

	@RestResource(exported = true)
	List<ModelDetails> findByEmpId(String empId);
	
	Page<ModelDetails> findAll(Pageable pageable);
	
	@RestResource(exported = true)
	List<ModelDetails> findByCompanyName(String companyName);
	

	@Override

	@RestResource(exported = false)
	ModelDetails save(ModelDetails arg0);

	@Override
	@RestResource(exported = false)
	default <S extends ModelDetails> List<S> save(Iterable<S> arg0) {
		// TODO Auto-generated method stub
		return null;
	}
}
