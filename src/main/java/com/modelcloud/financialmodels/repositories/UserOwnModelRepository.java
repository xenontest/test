package com.modelcloud.financialmodels.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.modelcloud.financialmodels.entities.UserOwnModel;


@RepositoryRestResource(collectionResourceRel = "userOwnModel", path = "user-models")
public interface UserOwnModelRepository extends MongoRepository<UserOwnModel, String> {
	
	@RestResource(exported = true)
	List<UserOwnModel> findByUserId(String userId);
	
	@RestResource(exported = true)
	List<UserOwnModel> findByUserIdAndModelId(String userId, String modelId);

}
