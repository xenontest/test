package com.modelcloud.financialmodels.repositories;

import java.util.List;
import java.util.concurrent.Future;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.modelcloud.financialmodels.entities.DownloadHistory;

@RepositoryRestResource(collectionResourceRel = "downloadHistory", path = "downloadHistory")
public interface DownloadHistoryRepository extends MongoRepository<DownloadHistory, String> {
	
	Future<List<DownloadHistory>> findByUserId(String userId);
	
	@RestResource(exported = true)
	DownloadHistory findByUserIdAndModelId(String userId, String modelId);
	
}
