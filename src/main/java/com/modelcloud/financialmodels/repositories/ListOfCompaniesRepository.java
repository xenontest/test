package com.modelcloud.financialmodels.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.modelcloud.financialmodels.entities.CsvFile;

@RepositoryRestResource(collectionResourceRel = "csvFile", path = "csvFile", exported = false)

public interface ListOfCompaniesRepository extends MongoRepository<CsvFile, String> {
	
	List<CsvFile> findBySymbolStartsWithOrNameStartsWith(String symbol, String name);

}
