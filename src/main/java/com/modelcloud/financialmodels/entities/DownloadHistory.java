package com.modelcloud.financialmodels.entities;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class DownloadHistory implements Serializable{

	@Id
	private String id;
	
	@NotNull
	String modelId;

	@NotNull
	String userId;

	@NotNull
	String filePath;

	@NotNull
	Date downloadedTime;
	
	@NotNull
	private String companyName;

	@NotNull
	private String title;

	@NotNull
	private String description;
	
	@NotNull
	private String modelName;

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getModelId() {
		return modelId;
	}

	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Date getDownloadedTime() {
		return downloadedTime;
	}

	public void setDownloadedTime(Date downloadedTime) {
		this.downloadedTime = downloadedTime;
	}
}
