package com.modelcloud.financialmodels.entities;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class UserOwnModel {

	@Id
	private String id;

	@NotNull
	String modelId;

	@NotNull
	String userId;

	@NotNull
	String filePath;
	
	@NotNull
	 String companyName;

	@NotNull
	 String title;
	
	 Date dateCreatedAt = new Date();
	 
		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getModelId() {
			return modelId;
		}

		public void setModelId(String modelId) {
			this.modelId = modelId;
		}

		public String getUserId() {
			return userId;
		}

		public void setUserId(String userId) {
			this.userId = userId;
		}

		public String getFilePath() {
			return filePath;
		}

		public void setFilePath(String filePath) {
			this.filePath = filePath;
		}

		public String getCompanyName() {
			return companyName;
		}

		public void setCompanyName(String companyName) {
			this.companyName = companyName;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public Date getDateCreatedAt() {
			return dateCreatedAt;
		}

		public void setDateCreatedAt(Date dateCreatedAt) {
			this.dateCreatedAt = dateCreatedAt;
		}
}
