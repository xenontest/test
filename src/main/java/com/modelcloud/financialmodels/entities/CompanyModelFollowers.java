package com.modelcloud.financialmodels.entities;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class CompanyModelFollowers {

	@Id
	private String id;
	
	@NotNull
	String modelId;

	@NotNull
	String userId;
	
	@NotNull
	String emailId;

	@NotNull
	Date dateFollowed=new Date();
	
	@NotNull
	String companyName;

	@NotNull
	String title;
	
	boolean alertActivated;
	
	boolean followed;

	public boolean isFollowed() {
		return followed;
	}

	public void setFollowed(boolean followed) {
		this.followed = followed;
	}

	public boolean isAlertActivated() {
		return alertActivated;
	}

	public void setAlertActivated(boolean alertActivated) {
		this.alertActivated = alertActivated;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getModelId() {
		return modelId;
	}

	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Date getDateFollowed() {
		return dateFollowed;
	}

	public void setDateFollowed(Date dateFollowed) {
		this.dateFollowed = dateFollowed;
	}
	
}
