package com.modelcloud.financialmodels.entities;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class AccessTokens {

	@Id
	private String id;

	@NotNull
	private String accessToken;
	@NotNull
	private Long expiresIn;
	@NotNull
	private String tokenType;
	@NotNull
	private String accessTokenSecret;
	@NotNull
	private String userId;
	@NotNull
	private String role;
	@NotNull
	private String apiKey;


	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Long getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(Long expiresIn) {
		this.expiresIn = expiresIn;
	}

	public String getTokenType() {
		return tokenType;
	}

	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}

	public AccessTokens() {
		// TODO Auto-generated constructor stub
	}

	public AccessTokens(String accessToken, String accessTokenSecret, String userId) throws Exception {
		super();
		this.accessToken = accessToken;
		this.accessTokenSecret = accessTokenSecret;
		this.userId = userId;
	}

	public String getAccessTokenSecret() {
		return accessTokenSecret;
	}

	public void setAccessTokenSecret(String accessTokenSecret) {
		this.accessTokenSecret = accessTokenSecret;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
}
