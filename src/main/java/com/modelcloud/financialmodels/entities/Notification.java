package com.modelcloud.financialmodels.entities;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Notification {

	@Id
	private String id;
	
	@NotNull
	String modelId;

	@NotNull
	String userId;
	
	@NotNull
	String companyName;

	@NotNull
	String title;
	
	@NotNull
	String comment;
	
	@NotNull
	boolean isEmailSent;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getModelId() {
		return modelId;
	}

	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public boolean isEmailSent() {
		return isEmailSent;
	}

	public void setEmailSent(boolean isEmailSent) {
		this.isEmailSent = isEmailSent;
	}
}
