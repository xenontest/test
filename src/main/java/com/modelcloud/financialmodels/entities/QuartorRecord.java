package com.modelcloud.financialmodels.entities;

import java.io.Serializable;
import java.util.List;

public class QuartorRecord implements Serializable{

	String quarter;

	String currentYear;

	List values;
	
//	String sheetName;
//
//	public String getSheetName() {
//		return sheetName;
//	}
//
//	public void setSheetName(String sheetName) {
//		this.sheetName = sheetName;
//	}

	public String getQuarter() {
		return quarter;
	}

	public void setQuarter(String quarter) {
		this.quarter = quarter;
	}

	public String getCurrentYear() {
		return currentYear;
	}

	public void setCurrentYear(String currentYear) {
		this.currentYear = currentYear;
	}

	public List getValues() {
		return values;
	}

	public void setValues(List values) {
		this.values = values;
	}
}
