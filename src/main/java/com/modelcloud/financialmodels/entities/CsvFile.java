package com.modelcloud.financialmodels.entities;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;

public class CsvFile {

	@Id
	String id;

	@Field("Symbol")

	String symbol;

	@Field("Name")
	String name;

	@Field("LastSale")
	String lastSale;

	@Field("MarketCap")
	String marketCap;

	@Field("IPOyear")
	String ipoYear;

	@Field("Sector")
	String sector;

	@Field("industry")
	String industry;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastSale() {
		return lastSale;
	}

	public void setLastSale(String lastSale) {
		this.lastSale = lastSale;
	}

	public String getMarketCap() {
		return marketCap;
	}

	public void setMarketCap(String marketCap) {
		this.marketCap = marketCap;
	}

	public String getIpoYear() {
		return ipoYear;
	}

	public void setIpoYear(String ipoYear) {
		this.ipoYear = ipoYear;
	}

	public String getSector() {
		return sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

}
