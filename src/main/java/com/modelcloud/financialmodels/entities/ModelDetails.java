package com.modelcloud.financialmodels.entities;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.google.gson.Gson;

@Document
public class ModelDetails implements Serializable {

	@Id
	private String uuid;

	@NotNull
	private String companyName;

	@NotNull
	private String title;

	@NotNull
	private String description;
	@NotNull
	private String modelPath;

	@NotNull
	private String empId;

	private boolean isDeleted;

	
	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getModelPath() {
		return modelPath;
	}

	public void setModelPath(String modelPath) {
		this.modelPath = modelPath;
	}

	@Override
	public String toString() {
		return new Gson().toJson(this);
	}

	private Date dateCreatedAt = new Date();

	public Date getDateCreatedAt() {
		return dateCreatedAt;
	}

	public void setDateCreatedAt(Date dateCreatedAt) {
		this.dateCreatedAt = dateCreatedAt;
	}

	// public void DisplayDate() {
	// date = new Date();
	// System.out.println(date);
	// }

}
