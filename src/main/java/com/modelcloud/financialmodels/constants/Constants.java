package com.modelcloud.financialmodels.constants;

public interface Constants {
	String MODELCLOUD_VARIABLE = "MODELCLOUD_VARIABLE";
	String BASE_PACKAGE_NAME = "com.modelcloud.financialmodels";
	String ACCESS_TOKEN = "modelcloudlogin";
}
