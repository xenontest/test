package com.modelcloud.financialmodels.controller;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Future;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.modelcloud.financialmodels.entities.DownloadHistory;
import com.modelcloud.financialmodels.entities.ModelDetails;
import com.modelcloud.financialmodels.repositories.DownloadHistoryRepository;
import com.modelcloud.financialmodels.utils.PropertiesFile;

@RepositoryRestController
public class DownloadHistoryController {

	@Autowired
	DownloadHistoryRepository downloadHistoryRepository;
	
	@Autowired
	PropertiesFile propertiesFile;

	@RequestMapping(method = RequestMethod.GET, value = "/downloadHistory/findByUserId")
	public @ResponseBody ResponseEntity<List<DownloadHistory>> searchUserModelHistory(
			@RequestParam("userId") String userId) throws Exception {
		Future<List<DownloadHistory>> result = downloadHistoryRepository.findByUserId(userId);
		while (!result.isDone()) {
		}
		List<DownloadHistory> list = result.get();
		return new ResponseEntity<List<DownloadHistory>>(list, HttpStatus.OK);
	}

//	@CrossOrigin
//	@RequestMapping(method = RequestMethod.POST, value = "/downloadHistory/{id}")
//	public ResponseEntity<DownloadHistory> updateFiles(@RequestParam(value = "file", required = false) MultipartFile file,
//			@PathVariable("id") String id, DownloadHistory downloadHistory) throws Exception {
//		
//		try {
//			DownloadHistory downloadHistoryDb=downloadHistoryRepository.findOne(id);
//			if (file != null && file.getSize() > 0) {
//				new File(downloadHistory.getFilePath()).delete();
//				byte[] bytes = file.getBytes();
//				String absoluteFilePath = propertiesFile.getDownloadedHistoryPath() + File.separator + downloadHistoryDb.getUserId() + File.separator
//						+ file.getOriginalFilename().replaceAll(" ", "");
//				File parentDir = new File(absoluteFilePath).getParentFile();
//				if (!parentDir.isDirectory()) {
//					parentDir.mkdirs();
//				}
//				Path path = Paths.get(absoluteFilePath);
//				Files.write(path, bytes);
//				downloadHistory.setFilePath(absoluteFilePath);
//			}
//			
//			
//			downloadHistoryDb.setFilePath(downloadHistory.getFilePath());
//			downloadHistoryDb.setDescription(downloadHistory.getDescription());
//			
//			downloadHistory = downloadHistoryRepository.save(downloadHistoryDb);
//			return new ResponseEntity<DownloadHistory>(downloadHistoryDb, HttpStatus.CREATED);
//		}catch (Exception e) {
//			e.printStackTrace();
//		}
//		return new ResponseEntity<DownloadHistory>(HttpStatus.BAD_REQUEST);
//	}
}
