package com.modelcloud.financialmodels.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.modelcloud.financialmodels.entities.CompanyModelFollowers;
import com.modelcloud.financialmodels.entities.ModelDetails;
import com.modelcloud.financialmodels.entities.User;
import com.modelcloud.financialmodels.repositories.ModelDetailRepository;
import com.modelcloud.financialmodels.repositories.ModelFollowerRepository;
import com.modelcloud.financialmodels.repositories.UserRepository;

@RepositoryRestController
public class ModelFollowersController {

	@Autowired
	ModelFollowerRepository modelFollowerRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	ModelDetailRepository modelDetailRepository;

	Map<String, String> errors = new HashMap<>();

	@RequestMapping(method = RequestMethod.GET, value = "/company-models-followers/findByUserId")
	public @ResponseBody ResponseEntity<List<CompanyModelFollowers>> search(@RequestParam("userId") String userId)
			throws Exception {
		List<CompanyModelFollowers> list = modelFollowerRepository.findByUserId(userId);
		return new ResponseEntity<List<CompanyModelFollowers>>(list, HttpStatus.OK);
	}

	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST, value = "/company-models-followers")
	public ResponseEntity<Object> modelFollowed(CompanyModelFollowers companyModelFollowers) {

		try {
			ModelDetails modelDetails = modelDetailRepository.findOne(companyModelFollowers.getModelId());
			if (modelDetails != null) {
				User user = userRepository.findOne(companyModelFollowers.getUserId());
				if (user != null) {
					if (companyModelFollowers.getDateFollowed() == null)
						companyModelFollowers.setDateFollowed(new Date());

					companyModelFollowers.setEmailId(user.getEmailId());

					companyModelFollowers = modelFollowerRepository.save(companyModelFollowers);
					return new ResponseEntity<Object>(companyModelFollowers, HttpStatus.CREATED);
				} else {
					errors.put("error", "User does not exist");
					return new ResponseEntity<Object>(errors, HttpStatus.BAD_REQUEST);
				}
			} else {
				errors.put("error", "Financial Model does not exist");
				return new ResponseEntity<Object>(errors, HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<Object>(HttpStatus.BAD_REQUEST);
	}

	@CrossOrigin
	@RequestMapping(method = RequestMethod.PATCH, value = "/company-models-followers/{id}")
	public ResponseEntity<Object> activateAlerting(@RequestBody CompanyModelFollowers companyModelFollowersRequest,
			@PathVariable("id") String id) {
		System.out.println("alertActivated = " + companyModelFollowersRequest.isAlertActivated());

		try {
				CompanyModelFollowers companyModelFollowers = modelFollowerRepository.findOne(id);
				if (companyModelFollowers != null) {
					companyModelFollowers.setAlertActivated(companyModelFollowersRequest.isAlertActivated());
					System.out.println(companyModelFollowers.isAlertActivated());
					companyModelFollowers = modelFollowerRepository.save(companyModelFollowers);
					return new ResponseEntity<Object>(companyModelFollowers, HttpStatus.OK);
				}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<Object>(HttpStatus.BAD_REQUEST);
	}
}
