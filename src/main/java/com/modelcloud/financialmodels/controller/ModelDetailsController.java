package com.modelcloud.financialmodels.controller;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.websocket.server.PathParam;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.modelcloud.financialmodels.dto.ModelsDto;
import com.modelcloud.financialmodels.dto.ModelsTabDto;
import com.modelcloud.financialmodels.entities.AccessTokens;
import com.modelcloud.financialmodels.entities.CompanyModelFollowers;
import com.modelcloud.financialmodels.entities.DownloadHistory;
import com.modelcloud.financialmodels.entities.ModelDetails;
import com.modelcloud.financialmodels.entities.Notification;
import com.modelcloud.financialmodels.entities.User;
import com.modelcloud.financialmodels.entities.UserOwnModel;
import com.modelcloud.financialmodels.repositories.AccessTokenRepository;
import com.modelcloud.financialmodels.repositories.DownloadHistoryRepository;
import com.modelcloud.financialmodels.repositories.ModelDetailRepository;
import com.modelcloud.financialmodels.repositories.ModelFollowerRepository;
import com.modelcloud.financialmodels.repositories.NotificationRepository;
import com.modelcloud.financialmodels.repositories.UserOwnModelRepository;
import com.modelcloud.financialmodels.repositories.UserRepository;
import com.modelcloud.financialmodels.utils.EmailUtils;
import com.modelcloud.financialmodels.utils.ExcelReaderUtil;
import com.modelcloud.financialmodels.utils.PropertiesFile;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.model.PutObjectRequest;

@RepositoryRestController
public class ModelDetailsController {

	@Autowired
	PropertiesFile propertiesFile;

	@Autowired
	ExcelReaderUtil excelReaderUtil;

	@Autowired
	ModelDetailRepository modelDetailRepository;

	@Autowired
	DownloadHistoryRepository downloadHistoryRepository;

	@Autowired
	ModelFollowerRepository modelFollowerRepository;

	@Autowired
	NotificationRepository notificationRepository;

	@Autowired
	UserOwnModelRepository userOwnModelRepository;

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	AccessTokenRepository accessTokenRepository;

	@Autowired
	EmailUtils emailUtils;

	final AmazonS3 amazonS3 = new AmazonS3Client();

	Map<String, String> errors = new HashMap<>();

	@RequestMapping(method = RequestMethod.GET, value = "/modeldetails/findByEmpId")
	public @ResponseBody ResponseEntity<List<ModelDetails>> search(@RequestParam("empId") String empId)
			throws Exception {
		List<ModelDetails> list = modelDetailRepository.findByEmpId(empId);
		return new ResponseEntity<List<ModelDetails>>(list, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/modeldetails/{id}")
	public @ResponseBody ResponseEntity<ModelDetails> get(@PathVariable("id") String id) throws Exception {
		ModelDetails modelDetails = modelDetailRepository.findOne(id);

		return new ResponseEntity<ModelDetails>(modelDetails, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/modeldetails/{id}/{sheetname}")
	public @ResponseBody ResponseEntity<ModelsTabDto> excelData(@PathVariable("id") String id,
			@PathVariable("sheetname") String sheetname) throws Exception {
		ModelDetails modelDetails = modelDetailRepository.findOne(id);
		ModelsTabDto modelsDto = new ModelsTabDto(modelDetails.getUuid(), modelDetails.getCompanyName(),
				modelDetails.getTitle(), modelDetails.getDescription(),
				excelReaderUtil.excelReader(modelDetails.getModelPath(), sheetname), modelDetails.isDeleted(),
				modelDetails.getDateCreatedAt());

		return new ResponseEntity<ModelsTabDto>(modelsDto, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/modeldetails/users/{userId}/findByCompanyName")
	public @ResponseBody List<ModelsDto> getFollowedModels(@PathVariable("userId") String userId,
			@RequestParam("companyName") String companyName) throws Exception {
		List<ModelsDto> modelDtoList = new ArrayList<ModelsDto>();
		if (companyName.equals("")) {

			PageRequest request = new PageRequest(0, 10, Sort.Direction.DESC, "dateCreatedAt");
			List<ModelDetails> modelDetailsList = modelDetailRepository.findAll(request).getContent();
			List<CompanyModelFollowers> followModelsList = modelFollowerRepository.findByUserId(userId);
			List<UserOwnModel> UserOwnModel = userOwnModelRepository.findByUserId(userId);

			for (ModelDetails modelDetails : modelDetailsList) {
				ModelsDto modelsDto = new ModelsDto(modelDetails.getUuid(), modelDetails.getCompanyName(),
						modelDetails.getTitle(), modelDetails.getDescription(), modelDetails.getDateCreatedAt());

				for (CompanyModelFollowers companyModelFollower : followModelsList) {
					if (modelsDto.getUuid().equals(companyModelFollower.getModelId())) {
						modelsDto.setFollowed(true);
						break;
					}
				}
				// for setting userownmodel is added or not.....
				for (UserOwnModel userModel : UserOwnModel) {
					if (modelsDto.getUuid().equals(userModel.getModelId())) {
						modelsDto.setUserOwnModelAdded(true);
						break;
					}
				}

				modelDtoList.add(modelsDto);
			}
		} else {
			List<ModelDetails> modelDetailsList = modelDetailRepository.findByCompanyName(companyName);
			List<CompanyModelFollowers> followModelsList = modelFollowerRepository.findByUserId(userId);

			for (ModelDetails modelDetails : modelDetailsList) {
				ModelsDto modelsDto = new ModelsDto(modelDetails.getUuid(), modelDetails.getCompanyName(),
						modelDetails.getTitle(), modelDetails.getDescription(), modelDetails.getDateCreatedAt());

				for (CompanyModelFollowers companyModelFollower : followModelsList) {
					if (modelsDto.getUuid().equals(companyModelFollower.getModelId())) {
						modelsDto.setFollowed(true);
						break;
					}
				}

				modelDtoList.add(modelsDto);
			}
		}
		return modelDtoList;

	}

	@RequestMapping(method = RequestMethod.GET, value = "/modeldetails/request/download/{userId}/{id}/{fileName}")
	public void downloadExcelData(@PathVariable("userId") String userId, @PathVariable("id") String id,@PathVariable("fileName") String fileName,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		System.out.println("Entered in an API");

		AccessTokens accessTokens = accessTokenRepository.findByUserId(userId);
		if(accessTokens!=null){
		if(accessTokens.getApiKey().equals(propertiesFile.getRegisteredUserApiKey()))
		{
		System.out.println("11111");
		ServletContext context = request.getServletContext();

		// construct the complete absolute path of the file
		ModelDetails modelDetails = modelDetailRepository.findOne(id);
		String fullPath = modelDetails.getModelPath();

		File downloadFile = new File(fullPath);
		FileInputStream inputStream = new FileInputStream(downloadFile);

	
		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setContentLength((int) downloadFile.length());

		// set headers for the response
		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"", downloadFile.getName());
		response.setHeader(headerKey, headerValue);

		// get output stream of the response
		OutputStream outStream = response.getOutputStream();

		byte[] buffer = new byte[1024];
		int bytesRead = -1;
		// write bytes read from the input stream into the output stream
		while ((bytesRead = inputStream.read(buffer)) != -1) {
			outStream.write(buffer, 0, bytesRead);
		}
		inputStream.close();
		outStream.close();
		}
		}
//		response.sendRedirect("/fin-models/"+modelDetails.getModelPath().substring(modelDetails.getModelPath().lastIndexOf(File.separator)));
	}

	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST, value = "/modeldetails")
	public ResponseEntity<Object> uploadFiles(@RequestParam("file") MultipartFile file, ModelDetails modelDetails) {
		try {
			User user = userRepository.findOne(modelDetails.getEmpId());
			if (user != null) {
				byte[] bytes = file.getBytes();
//				String absoluteFilePath = propertiesFile.getFinancialUploadedModels() + File.separator
//						+ (new Date().getTime() + "_" + file.getOriginalFilename().replaceAll(" ", ""));
				
				String absoluteFilePath = propertiesFile.getFinancialUploadedModels() + File.separator
						+ (new Date().getTime() + "_" + file.getOriginalFilename().replaceAll(" ", ""));
				File parentDir = new File(absoluteFilePath).getParentFile();
				if (!parentDir.isDirectory()) {
					parentDir.mkdirs();
				}
				Path path = Paths.get(absoluteFilePath);
				Files.write(path, bytes);
				System.out.println(absoluteFilePath);
				System.out.println(path);
				modelDetails.setModelPath(absoluteFilePath);
				if (modelDetails.getDateCreatedAt() == null)
					modelDetails.setDateCreatedAt(new Date());
				// modelDetails.setDeleted(0);
				// modelDetails.setStatus(0);
				modelDetails = modelDetailRepository.save(modelDetails);
				return new ResponseEntity<Object>(modelDetails, HttpStatus.CREATED);
			} else {
				errors.put("error", "Employee does not exist");
				return new ResponseEntity<Object>(errors, HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<Object>(HttpStatus.BAD_REQUEST);
	}

	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST, value = "/modeldetails/{id}")
	public ResponseEntity<Object> updateFiles(@RequestParam(value = "file", required = false) MultipartFile file,
			@PathVariable("id") String id, ModelDetails modelDetails, @RequestParam("comment") String comment)
			throws Exception {

		try {
			User user = userRepository.findOne(modelDetails.getEmpId());
			if (user != null) {
				if (file != null && file.getSize() > 0) {
					new File(modelDetails.getModelPath()).delete();
					byte[] bytes = file.getBytes();
					String absoluteFilePath = propertiesFile.getFinancialUploadedModels() + File.separator
							+ (new Date().getTime() + "_" + file.getOriginalFilename().replaceAll(" ", ""));
					File parentDir = new File(absoluteFilePath).getParentFile();
					if (!parentDir.isDirectory()) {
						parentDir.mkdirs();
					}
					Path path = Paths.get(absoluteFilePath);
					Files.write(path, bytes);
					modelDetails.setModelPath(absoluteFilePath);
				}
				ModelDetails modelDetailsDB = modelDetailRepository.findOne(id);

				modelDetailsDB.setModelPath(modelDetails.getModelPath());
				modelDetailsDB.setCompanyName(modelDetails.getCompanyName());
				modelDetailsDB.setTitle(modelDetails.getTitle());
				modelDetailsDB.setEmpId(modelDetails.getEmpId());
				modelDetailsDB.setDescription(modelDetails.getDescription());

				modelDetails = modelDetailRepository.save(modelDetailsDB);

				Notification notification = new Notification();
				// List<CompanyModelFollowers> companyModelFollowers =
				// modelFollowerRepository.findByModelId(id);
				List<CompanyModelFollowers> companyModelFollowers = modelFollowerRepository
						.findByModelIdAndAlertActivated(id, true);
				for (CompanyModelFollowers modelFollowers : companyModelFollowers) {
					notification.setCompanyName(modelFollowers.getCompanyName());
					notification.setTitle(modelFollowers.getTitle());
					notification.setUserId(modelFollowers.getUserId());
					notification.setModelId(modelFollowers.getModelId());
					notification.setComment(comment);
					notification.setEmailSent(true);
					notification = notificationRepository.save(notification);

					try {

						emailUtils.sendMail(modelFollowers.getCompanyName(), modelFollowers.getTitle(),
								modelFollowers.getEmailId(), comment);

					} catch (Exception e) {
						e.printStackTrace();
					}

				}

				return new ResponseEntity<Object>(modelDetailsDB, HttpStatus.CREATED);
			} else {
				errors.put("error", "Employee does not exist");
				return new ResponseEntity<Object>(errors, HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<Object>(HttpStatus.BAD_REQUEST);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/modeldetails/findByCompanyName")
	public @ResponseBody ResponseEntity<List<ModelDetails>> searchCompanies(
			@RequestParam("companyName") String companyName) throws Exception {
		List<ModelDetails> list = modelDetailRepository.findByCompanyName(companyName);
		return new ResponseEntity<List<ModelDetails>>(list, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/modeldetails/request/save-to-drive/{id}")
	public void saveToDrive(@RequestParam("userId") String userId, @PathVariable("id") String id,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		System.out.println("Entered in an saveToCloud API");

		// ServletContext context = request.getServletContext();

		// construct the complete absolute path of the file
		// DownloadHistory result =
		// downloadHistoryRepository.findByUserIdAndModelId(userId, id);
		// System.out.println(result);
		// if(result==null)
		// {

		ModelDetails modelDetails = modelDetailRepository.findOne(id);

		String fullPath = modelDetails.getModelPath();

		File downloadFile = new File(fullPath);
		FileInputStream inputStream = new FileInputStream(downloadFile);
		//
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		org.apache.commons.io.IOUtils.copy(inputStream, byteArrayOutputStream);
		byte[] bytes = byteArrayOutputStream.toByteArray();

		response.setContentLength((int) downloadFile.length());
		DownloadHistory result = downloadHistoryRepository.findByUserIdAndModelId(userId, id);
		System.out.println(result);
		if (result == null) {

			// set headers for the response
			// String headerKey = "Content-Disposition";
			// String headerValue = String.format("attachment; filename=\"%s\"",
			// downloadFile.getName());
			// response.setHeader(headerKey, headerValue);

			// get output stream of the response
			// OutputStream outStream = response.getOutputStream();

			byte[] buffer = new byte[1024];
			int bytesRead = -1;
			// ByteArrayInputStream byteArrayInputStream = new
			// ByteArrayInputStream(bytes);
			// write bytes read from the input stream into the output stream
			// while ((bytesRead = byteArrayInputStream.read(buffer)) != -1) {
			//// outStream.write(buffer, 0, bytesRead);
			// }
			User user = userRepository.findOne(userId);

			String absoluteFilePath = propertiesFile.getDownloadedHistoryPath() + File.separator + user.getEmailId()
					+ File.separator + downloadFile.getName();
			File parentDir = new File(absoluteFilePath).getParentFile();

			if (!parentDir.isDirectory()) {
				parentDir.mkdirs();
			}

			ByteArrayInputStream historyByteArrayInputStream = new ByteArrayInputStream(bytes);
			try (FileOutputStream fileOutputStream = new FileOutputStream(absoluteFilePath)) {
				while ((bytesRead = historyByteArrayInputStream.read(buffer)) != -1) {
					fileOutputStream.write(buffer, 0, bytesRead);
				}

				DownloadHistory downloadHistory = new DownloadHistory();
				downloadHistory.setFilePath(absoluteFilePath);
				downloadHistory.setModelId(id);
				downloadHistory.setUserId(userId);
				downloadHistory.setModelName(modelDetails.getCompanyName() + "_" + user.getEmailId());
				if (downloadHistory.getDownloadedTime() == null)
					downloadHistory.setDownloadedTime(new Date());

				downloadHistory.setCompanyName(modelDetails.getCompanyName());
				downloadHistory.setTitle(modelDetails.getTitle());
				downloadHistory.setDescription(modelDetails.getDescription());

				downloadHistory = downloadHistoryRepository.save(downloadHistory);

				fileOutputStream.close();
				
				System.out.println("Started S3");
				String bucketName     = propertiesFile.getAwsS3BucketName();
				String keyName        = user.getEmailId();
				String uploadFileName = modelDetails.getCompanyName() + "_" + user.getEmailId();
				System.out.println(downloadHistory.getFilePath());
				File uploadFile = new File(downloadHistory.getFilePath()); 
			
				try {
					amazonS3.putObject(new PutObjectRequest(bucketName, keyName, downloadFile ));
				} catch (Exception e) {
					e.printStackTrace();
				}
				System.out.println("Uploaded to s3 cloud :)");

			}
			
			
//			String bucketName     = "jeffy";
//			String keyName        = user.getEmailId();
//			String uploadFileName = modelDetails.getCompanyName() + "_" + user.getEmailId();
//			 
//			AmazonS3 s3client = new AmazonS3Client(new ProfileCredentialsProvider());
//			try {
//				s3client.putObject(new PutObjectRequest(bucketName, keyName, downloadFile ));
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//			System.out.println("Uploaded to s3 cloud :)");
			// outStream.close();
		}

	}

}
