package com.modelcloud.financialmodels.controller;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.modelcloud.financialmodels.entities.ModelDetails;
import com.modelcloud.financialmodels.entities.User;
import com.modelcloud.financialmodels.entities.UserOwnModel;
import com.modelcloud.financialmodels.repositories.ModelDetailRepository;
import com.modelcloud.financialmodels.repositories.UserOwnModelRepository;
import com.modelcloud.financialmodels.repositories.UserRepository;
import com.modelcloud.financialmodels.utils.PropertiesFile;

@RepositoryRestController
public class UserOwnModelController {

	@Autowired
	UserOwnModelRepository userOwnModelRepository;

	@Autowired
	PropertiesFile propertiesFile;

	@Autowired
	ModelDetailRepository modelDetailRepository;

	@Autowired
	UserRepository userRepository;

	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST, value = "/user-models")
	public ResponseEntity<Object> uploadFiles(@RequestParam("file") MultipartFile file, UserOwnModel userOwnModel) {

		try {
			Map<String, String> errors = new HashMap<>();

			ModelDetails modelDetails = modelDetailRepository.findOne(userOwnModel.getModelId());
			if (modelDetails != null) {
				User userEntity = userRepository.findOne(userOwnModel.getUserId());
				if (userEntity != null) {
					List<UserOwnModel> user = userOwnModelRepository.findByUserId(userOwnModel.getUserId());
					if (user.size() < 100) {
						byte[] bytes = file.getBytes();
						double sizeBytes = file.getSize();
						List<UserOwnModel> UserModel = userOwnModelRepository
								.findByUserIdAndModelId(userOwnModel.getUserId(), userOwnModel.getModelId());
						if (UserModel != null && UserModel.size() > 0) {
							errors.put("errors", "Already added a proprietary model");
							return new ResponseEntity<Object>(errors, HttpStatus.BAD_REQUEST);
						} else {
							if (sizeBytes <= 2097152) {
								String absoluteFilePath = propertiesFile.getUserOwnModel() + File.separator
										+ (new Date().getTime() + "_" + file.getOriginalFilename().replaceAll(" ", ""));
								File parentDir = new File(absoluteFilePath).getParentFile();
								if (!parentDir.isDirectory()) {
									parentDir.mkdirs();
								}
								Path path = Paths.get(absoluteFilePath);
								Files.write(path, bytes);
								System.out.println(absoluteFilePath);
								System.out.println(path);
								userOwnModel.setFilePath(absoluteFilePath);
								if (userOwnModel.getDateCreatedAt() == null)
									userOwnModel.setDateCreatedAt(new Date());
								userOwnModel = userOwnModelRepository.save(userOwnModel);
								return new ResponseEntity<Object>(userOwnModel, HttpStatus.CREATED);
							} else {
								errors.put("errors", "File size should be less than 2MB");
								return new ResponseEntity<Object>(errors, HttpStatus.BAD_REQUEST);
							}
						}
					}
				} else {
					errors.put("error", "User does not exist");
					return new ResponseEntity<Object>(errors, HttpStatus.BAD_REQUEST);
				}
			} else {
				errors.put("error", "Financial Model does not exist");
				return new ResponseEntity<Object>(errors, HttpStatus.BAD_REQUEST);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<Object>(HttpStatus.BAD_REQUEST);

	}

}
