package com.modelcloud.financialmodels.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.modelcloud.financialmodels.dao.CompanyNamesDao;
import com.modelcloud.financialmodels.entities.CsvFile;
import com.modelcloud.financialmodels.repositories.ListOfCompaniesRepository;

@Controller
public class LoadCompanyNames {

	@Autowired
	CompanyNamesDao companyNamesDao;
	@Autowired
	ListOfCompaniesRepository listOfCompaniesRepository;

	@RequestMapping(method = RequestMethod.GET, value = "/modeldetails/loadAllCompanies")
	public @ResponseBody ResponseEntity<List<String>> loadCompanies(
			@RequestParam(value = "companyName", required = false) String companyName) throws Exception {
		List<CsvFile> result = listOfCompaniesRepository.findAll();

		List<String> companies=new ArrayList<>();
		for (CsvFile csvFile : result) {
			companies.add(csvFile.getSymbol()+" | "+csvFile.getName());
		}
		// BySymbolStartsWithOrNameStartsWith(companyName, companyName);
//		System.out.println("result" + companies);
		return new ResponseEntity<List<String>>(companies, HttpStatus.OK);
	}

}
