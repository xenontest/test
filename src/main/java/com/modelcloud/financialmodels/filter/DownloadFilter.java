package com.modelcloud.financialmodels.filter;

import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.google.gson.Gson;
import com.modelcloud.financialmodels.constants.Constants;
import com.modelcloud.financialmodels.entities.AccessTokens;
import com.modelcloud.financialmodels.repositories.AccessTokenRepository;
import com.modelcloud.financialmodels.utils.PropertiesFile;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

@WebFilter(urlPatterns = { /*"/modeldetails/request/download/*" */ "/modeldetails/request/save-to-drive/*"/*, "/downloadHistory/*"*/})
public class DownloadFilter implements Filter, Constants {

	PropertiesFile propertiesFile;

	AccessTokenRepository accessTokenRepository;

	Map<String, Object> mapResponse = new HashMap<String, Object>();

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		WebApplicationContext springContext = WebApplicationContextUtils
				.getWebApplicationContext(arg0.getServletContext());
		propertiesFile = (PropertiesFile) springContext.getBean("propertiesFile");
		accessTokenRepository=(AccessTokenRepository) springContext.getBean("accessTokenRepository");

	}

	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain arg2)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) arg0;
		HttpServletResponse response = (HttpServletResponse) arg1;
		try {
			System.out.println("Enter in filter");
			System.out.println(propertiesFile.getRegisteredUserApiKey());
			

			Cookie[] cookies = request.getCookies();
			System.out.println("cookies = "+request.getCookies());
			String apiKey = "";
			if (cookies != null) {
				for (Cookie cookie : cookies) {
					if (cookie.getName().equals(ACCESS_TOKEN)) {
						System.out.println("Cookie value = "+cookie.getValue());
						List<AccessTokens> accessToken = accessTokenRepository.findByAccessToken(cookie.getValue());
						if (accessToken != null && accessToken.size() > 0) {
							apiKey=accessToken.get(0).getApiKey();
						}
					}
				}
			}
			// System.out.println(request.getHeaderNames().toString());
			// Enumeration<String> enumS=request.getHeaderNames();
			// while (enumS.hasMoreElements()){
			// String element = enumS.nextElement();
			// System.out.println(element);
			// }
			if (propertiesFile.getRegisteredUserApiKey().equals(apiKey)) {
				arg2.doFilter(arg0, arg1);
				System.out.println("1111");
			} else {
				System.out.println("2222");
				mapResponse = new HashMap<String, Object>();
				mapResponse.put("code", "400");
				mapResponse.put("status", "Bad Request");
				PrintWriter out = arg1.getWriter();
				out.println(new Gson().toJson(mapResponse));
				out.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void destroy() {

	}

}
