package com.modelcloud.financialmodels.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.modelcloud.financialmodels.entities.QuartorRecord;

public class ModelsTabDto implements Serializable {

	private String uuid;

	private String companyName;

	private String title;

	private String description;

	private Map data;

	public ModelsTabDto(String uuid, String companyName, String title, String description, 
			Map data, boolean isDeleted, Date dateCreatedAt) {
		super();
		this.uuid = uuid;
		this.companyName = companyName;
		this.title = title;
		this.description = description;
		this.data = data;
		this.isDeleted = isDeleted;
		this.dateCreatedAt = dateCreatedAt;
	}

	public Map getData() {
		return data;
	}

	public void setData(Map data) {
		this.data = data;
	}

	private boolean isDeleted;

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	@Override
	public String toString() {
		return new Gson().toJson(this);
	}

	private Date dateCreatedAt = new Date();

	public Date getDateCreatedAt() {
		return dateCreatedAt;
	}

	public void setDateCreatedAt(Date dateCreatedAt) {
		this.dateCreatedAt = dateCreatedAt;
	}

}
