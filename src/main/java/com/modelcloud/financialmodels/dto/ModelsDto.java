package com.modelcloud.financialmodels.dto;

import java.io.Serializable;
import java.util.Date;

import com.google.gson.Gson;

public class ModelsDto implements Serializable {

	private String uuid;

	private String companyName;

	private String title;

	private String description;

	private boolean isFollowed;
	
	private boolean isUserOwnModelAdded;

	public boolean isUserOwnModelAdded() {
		return isUserOwnModelAdded;
	}

	public void setUserOwnModelAdded(boolean isUserOwnModelAdded) {
		this.isUserOwnModelAdded = isUserOwnModelAdded;
	}

	public ModelsDto(String uuid, String companyName, String title, String description, Date dateCreatedAt) {
		super();
		this.uuid = uuid;
		this.companyName = companyName;
		this.title = title;
		this.description = description;
		this.dateCreatedAt = dateCreatedAt;
	}

	public boolean isFollowed() {
		return isFollowed;
	}

	public void setFollowed(boolean isFollowed) {
		this.isFollowed = isFollowed;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	@Override
	public String toString() {
		return new Gson().toJson(this);
	}

	private Date dateCreatedAt = new Date();

	public Date getDateCreatedAt() {
		return dateCreatedAt;
	}

	public void setDateCreatedAt(Date dateCreatedAt) {
		this.dateCreatedAt = dateCreatedAt;
	}
}
