package com.modelcloud.financialmodels.dao;

import java.util.List;

import com.modelcloud.financialmodels.entities.ModelDetails;


public interface CompanyNamesDao {
	
	List<ModelDetails> findcompanyNames();

}
