package com.modelcloud.financialmodels.dao.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.data.mongodb.core.query.Query;

import com.modelcloud.financialmodels.dao.CompanyNamesDao;

@Repository
public class CompanyNamesDaoImpl implements CompanyNamesDao {

	@Autowired
	private MongoTemplate mongoOperation;

	@Override
	public List findcompanyNames() {
		
		Query query = new Query();
		List list1 = mongoOperation.getCollection("csvFile").distinct("Symbol",query.getQueryObject());
		List list2 = mongoOperation.getCollection("csvFile").distinct("Name",query.getQueryObject());
		

		final Iterator<String> i1 = list1.iterator();
		final Iterator<String> i2 = list2.iterator();

		List<String> combined = new ArrayList<>();

		while (i1.hasNext() && i2.hasNext()) {
			combined.add(i1.next() + " | " + i2.next());
		}

//		System.out.println(combined);

		return combined;
	}
}
