FROM 10.233.26.159:5000/maven:3-alpine

MAINTAINER  Davinder Pal Singh "davinder@xenondigilabs.com"

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Copying src code to Container
COPY . /usr/src/app

# Building From Source Code
RUN mvn clean package

# Exposing ports to external systems
EXPOSE 7103

CMD ["java", "-jar", "target/ModelModuleProject-0.0.1-SNAPSHOT.jar"]

